#include "eq_client1.hpp"
#include "eq_net_auth.hpp"
#include <log.hpp>
#include <text_help.hpp>

#define MLOG(A) _log() << _log::_set_module_level("eq_client1",(A))

#define DEBUG_LEVEL             10
#define DEBUG_LEVEL_HIGH        16

_client1::_client1(const string &server,int port, const string &username, const string &password)
    :_eq_net_client(server,port)
    ,cm_user_name(username)
    ,cm_user_pass(password)
{

}

_client1::~_client1()
{
    if (timer)
    {
        timer->Kill();
        while(timer->GetStatus()!=TH_STATUS_KILLED) Sleep(10);
        delete timer;
        timer=NULL;
    }

    disconnect();
    if (nc) nc->release();
    nc=NULL;

    if (dc) dc->release();
    dc=NULL;
}

void _client1::try_connect()
{
    _eq_net_client::try_connect(); // first connect by tcp

    // Set _eq_net_client::cm_uname, _eq_net_client::cm_upass.
    // Invoke try_login().
    if (cm_status==_eq_net_client::client_status_login_need)
    {
        if (login(cm_user_name,cm_user_pass)!=_eq_err::ok)
        {
            MLOG(0) << "Can not login to '" << _eq_net_client::cm_ip  << "' as " << cm_user_name << ". Wait 5 minutes" << endl;
            unlock();
            Sleep(1000*5*60);
            lock();
        }
    }
    else if (cm_status!=_eq_net_client::client_status_ok)
    {
        MLOG(0) << "Can not connect to office '" << _eq_net_client::cm_ip << "'. Status = " << (int)_eq_net_client::cm_status << ". Wait 5 minutes" << endl;
        unlock();
        Sleep(1000*5*60);
        lock();
    }

    if (cm_status==_eq_net_client::client_status_ok)
    {
        // we must change status to do_connect (because get arc_events can be long operation)
        cm_status=_eq_net_client::client_status_do_connect;


// COMPATIBLE

        //lock(); ///
        /*int proc_cnt=1;
        while(proc_cnt)
        {
            proc_cnt=arc_events();
            if (proc_cnt<0)
            {
                disconnect();
                unlock();
                Sleep(1000*60*5);   // 5 min
                return;
            }
        }*/
        //unlock();
        //Sleep(1000*5); // 5 sec
// COMPATIBLE END


        cm_status=_eq_net_client::client_status_ok;
    }
}

void _client1::try_login()
{
    if (cm_status!=_eq_net_client::client_status_login_need) return;
    if (!nc)
    {
        cm_status=_eq_net_client::client_status_do_connect;
        return;
    }

    _eq_auth *auth=NULL;
    string rez;
    try
    {
        MLOG(0) << "Auth as " << cm_uname << endl;
        auth=create_net_eq_auth(nc);

        int level;
        _acls_list allow;
        _acls_list deny;

        bool ares=auth->auth(cm_uname,cm_upass,level,allow,deny);
        auth->release();
        auth=NULL;
        if (ares)
        {
            MLOG(0) << "Auth - OK" << endl;
        }
        else
        {
            MLOG(0) << "Auth ...wrong pass or user" << endl;
            nc->release();
            nc=NULL;
            cm_status=_eq_net_client::client_status_do_connect;
            return;
        }

        nc->cmd("get_param data_port",rez);
        if (rez[0]!='+')
        {
            MLOG(0) << "Can not get data port value from server..." << endl;
            nc->release();
            nc=NULL;
            cm_status=_eq_net_client::client_status_do_connect;
            return;
        }

        cm_data_port_str=_text_help::get_field_from_st(rez,"=;",1);

        get_jsondump();
    }
    catch(_nerror &e)
    {
        MLOG(0) << "Try_login - " << e.err_st_full() << endl;
        if (auth) auth->release();
        if (nc) nc->release();
        nc=NULL;
        return ;
    }
    catch(_eq_auth_error &e)
    {
        MLOG(0) << "Try_login - auth error: " << e.err_str() << endl;
        if (auth) auth->release();
        if (nc) nc->release();
        nc=NULL;
        return ;
    }

    // Create connection to 47021.
    if (!dc)
    {
        try
        {
            dc=create_net_datagram_client();
            MLOG(0) << "Try_login (DC) - connect to " << cm_ip << ":" << cm_data_port_str << endl;
            dc->connect(_tcp_adress(cm_ip,cm_data_port_str));
            MLOG(0) << "Try_login (DC) - OK" << endl;
        }
        catch(_nerror &e)
        {
            MLOG(0) << "Try_login (DC) - " << e.err_st_full() << endl;
            if (dc) dc->release();
            dc=NULL;
            return ;
        }
    }


    if(cm_status==_eq_net_client::client_status_do_connect)
        return;

    // Subscribe to events.
    char tmp[256];
    try
    {
        MLOG(0) << "Try_login (DC) - start event stream " << cm_ip << ":" << cm_data_port_str << endl;
        sprintf(tmp,"stream_events start %u",dc->connection_id());
        nc->cmd(tmp,rez);
        if (rez[0]!='+')
        {
            MLOG(0) << "Try_login (DC) - server do not start stream events " << rez << endl;
            if (dc) dc->release();
            dc=NULL;
            return ;
        }
        MLOG(0) << "Try_login (DC) - start event stream - OK" << endl;
    }
    catch(_nerror &e)
    {
        MLOG(0) << "Try_login (DC) - " << e.err_st_full() << endl;
        if (dc) dc->release();
        dc=NULL;
        return ;
    }

    cm_status=_eq_net_client::client_status_ok;
}


int _client1::poll()
{
    if (cm_status==_eq_net_client::client_status_auth_err ||
            cm_status==_eq_net_client::client_status_dup_reg ||
            cm_status==_eq_net_client::client_status_no_such_wp ||
            cm_status==_eq_net_client::client_status_wp_busy)
    {
        Sleep(100);
        return 0;
    }

    if (cm_status==_eq_net_client::client_status_do_connect)
    {
        try_connect();
    }

    if (cm_status==_eq_net_client::client_status_login_need)
    {
        if (cm_uname.empty()) { Sleep(100); return 0; }
        try_login();
    }

    if (cm_status==_eq_net_client::client_status_ok)
    {
        try
        {
            dc->recv();
            _datagram *sd=dc->datagram();
            if (sd)
            {
                if (analize_event(string((char *)sd->raw_data(),sd->raw_size()))) get_jsondump();
                delete sd;
            }
        }
        catch(_nerror &e)
        {
            MLOG(0) << "_eq_net_client::poll - recv datagram - " << e.err_st_full() << endl;
            disconnect();
            return 0;
        }

#ifndef _NO_ALIVE_SEND_
        try
        {
            _time now;
            now.set_now();
            now.tick(-1000*60); // 60 s = 1 minute

            if (now>cm_last_cmd_send_time)
            {
                MLOG(DEBUG_LEVEL) << "_eq_net_client::poll - send client_alive" << endl;
                string rez;
                _eq_err::_code err=cmd_to_server("client_alive",rez);
                if (err!=_eq_err::ok)
                {
                    MLOG(0) << "_eq_net_client::poll - send alive cmd - server return " << rez << endl;
                    disconnect();
                    return 0;
                }
            }
        }
        catch(_nerror &e)
        {
            MLOG(0) << "_eq_net_client::poll - send alive cmd - " << e.err_st_full() << endl;
            disconnect();
            return 0;
        }
#endif
    }

    return 0;
}


void _client1::get_jsondump()
{
    string strjson;
    try
    {
        MLOG(4) << "eqnc::get_jsondump() - get_json_dump" << endl;
        nc->cmd("get_json_dump",strjson);
    }
    catch(_nerror &e)
    {
        MLOG(0) << "Error - get_jsondump - get_json_dump" << endl; //e.err_st_full() << endl;
        disconnect();
        return;
    }

    if (strjson.size()<50)
    {
        MLOG(0) << "get_jsondump - json size (" << strjson.size() << "b) is too small" << endl;
        cm_status=_eq_net_client::client_status_xml_err;
        disconnect();
        return;
    }
}

