#if defined(WINDOWS)
        #include <windows.h>

        #if !defined(AS_APP)
        #define AS_SERVICE
        #endif

#else
        #define MAX_PATH        255
#endif

#include <iostream>
#include <cstdlib>
//BOOST
#include <boost/lambda/lambda.hpp>
#include <algorithm>

#include "../eq/eq_common/eq_common.hpp"
//#include <algorithm>
#include <log_file_writer.hpp> //proshin. For writing log to file
#include <error.hpp> //proshin error class for try() catch
#include <log.hpp>  //proshin log
#include <udefs.hpp> // DEL()

#include "client_global.hpp"
#include <config_parser.hpp>
#include <text_help.hpp>

#include "eq_client1.hpp"


// Macro
#define LPROG(A) _log::_set_module_level("prog",(A))

#define MLOG(A) _log() << _log::_set_module_level("prog",(A))

_log gLog;

_client1* pClient;

int main(int n,char *p[]);
int init();
int main_loop(int params_n,char *params[]);
int shutdown();
std::string human_readable_time(const _time &t);


class _config_file:public _config_parser_from_file
{
public:
    _config_file() {}
    virtual ~_config_file() {}

    virtual void parse_done() const;
};


// ----------------------------------------------
void _config_file::parse_done() const
{

    _config_parser::_params_map::const_iterator it=cm_params_map.begin();
    while(it!=cm_params_map.end())
    {
        if(it->first=="server")
        {
            _monitor_status().run_config().server_ip=_text_help::get_field_from_st(it->second,':',0);
            _monitor_status().run_config().server_port=_text_help::get_field_from_st(it->second,':',1);
        }
        else if (it->first=="log_levels") _monitor_status().run_config().log_levels=it->second;
        else if (it->first=="log_file") _monitor_status().run_config().log_file=it->second;
        else if (it->first=="user") _monitor_status().run_config().uname=it->second;
        else if (it->first=="pass") _monitor_status().run_config().upass=it->second;
        /*else if (it->first=="template_file") _monitor_status().run_config().template_file_name=it->second;
        else if (it->first=="scene_idle") _monitor_status().run_config().scene_idle=atoi(it->second.c_str());
        else if (it->first=="print_scene_idle") _monitor_status().run_config().print_scene_idle=atoi(it->second.c_str());
        else if (it->first=="msg_scene_idle") _monitor_status().run_config().msg_scene_idle=atoi(it->second.c_str());
        else if (it->first=="printer_name") _monitor_status().run_config().printer_name=it->second;*/
        else
        {
            throw(_error(-1,string("Неизвестный параметр ")+it->first+" в локальном файле настроек."));
        }

        it++;
    }

    if (_monitor_status().run_config().server_ip.empty()) throw _error(-1,"В параметрах не указан ip сервера (server)");
    if (_monitor_status().run_config().server_port.empty()) throw _error(-1,"В параметрах не указан port сервера (server)");
    if (_monitor_status().run_config().log_file.empty()) throw _error(-1,"В параметрах не указан файл протокола (log_file)");
    if (_monitor_status().run_config().log_levels.empty()) throw _error(-1,"В параметрах не указаны уровни протоколирования (log_levels)");
    if (_monitor_status().run_config().uname.empty()) throw _error(-1,"В параметрах не указан пользователь (user)");
    if (_monitor_status().run_config().upass.empty()) throw _error(-1,"В параметрах не указан пароль (pass)");

    // TODO
    int pn=0;
    while(1)
    {
        string pv=_text_help::get_field_from_st(_monitor_status().run_config().log_levels,";,",pn);
        if (pv.empty()) break;

        string par=_text_help::get_field_from_st(pv,':',0);
        string val=_text_help::get_field_from_st(pv,':',1);

        gLog.max_level4module(par,atoi(val.c_str()));

        pn++;
    }

}


// ----------------------------------------------------------
int main(int n,char *p[])
{
    if (n==1) // no params
    {
        _monitor_status().run_config().run_path="/etc/eq/";
        _monitor_status().run_config().config_file_name="/etc/eq/eq_client1.conf";
    }
    /*else if (n==2)
    {        

    }*/
    else
    {
        printf("Use without params.\n!");
        //lprintf("eq_lserver::too many params\nUsage: eq_lserver [config_file_name]\n");
        return -1;
    }

    // TODO demonize

    return main_loop(n,p);
}



// ----------------------------------------------------------
int main_loop(int params_n,char *params[])
{
    init();
    while(1)
    {


    }

    shutdown();

    return 0;
}



// ----------------------------------------------------------
int init()
{
    try
    {

        gLog.set_writer(new _log_file_writer("/var/log/eq/eq_client.log"));  //delete in shutdown();
        gLog.max_level4module("prog",15);

        MLOG(0) << "____Start: " << endl;



        try
        {
            _config_file *cf=new _config_file;
            cf->load(_monitor_status().run_config().config_file_name);
            delete cf;
        }
        catch(_error &e)
        {
            cout << e.str() << endl;
            _monitor_status().need_exit(true);
            return -1;
        }

        if (_monitor_status().run_config().server_ip!="do_not_connect")
        {
            pClient = new _client1(
                        _monitor_status().run_config().server_ip,
                        std::stoi(_monitor_status().run_config().server_port),
                        _monitor_status().run_config().uname,
                        _monitor_status().run_config().upass
                        );

        }
        else _monitor_status().qserver_client(NULL);




    }
    catch(_error &e)
    {
        if (gLog.get_writer())
        {
            char t[MAX_PATH*2];
            sprintf(t,"Error : %s. Code = %i;",e.str().c_str(),e.num());
            gLog << LPROG(0) << "Error: " << e.str() << ". Code = " << e.num() << endl;
        }
        else
        {
            printf("eq_client::init error:\n%s\n",e.str().c_str());
            return -1;
        }

    }

    return 0;
}


// -----------------------
int shutdown()
{
    try
    {
        pClient->release();

        delete gLog.get_writer();
        gLog.set_writer(NULL);

    }
    catch(_error &e)
    {
        //gLog << LPROG(0) << "Error while shutdown: " << e.str() << endl;
        MLOG(0) << "Error while shutdown: " << e.str() << endl;
    }


    return 0;
}


std::string human_readable_time(const _time &t)
{
    string st;
    st=to_std_string(t.year());
    st+=".";
    st+=to_std_string(t.month());
    st+=".";
    st+=to_std_string(t.day());
    st+=" ";
    st+=to_std_string(t.hour());
    st+=":";
    st+=to_std_string(t.minute());
    st+=":";
    st+=to_std_string(t.second());
    st+=".";
    st+=to_std_string(t.msecond());
    return st;
}
