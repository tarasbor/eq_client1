TEMPLATE = app
#CONFIG += console
#CONFIG += windows
CONFIG -= app_bundle
CONFIG -= qt

TARGET = eq_client1

#CONFIG(windows) {
#    DEFINES *= WINDOWS
#}
DEFINES -= UNICODE
DEFINES *= DB_USE_COMMON_TIME
DEFINES *= TCP_SERVER_TO_REINIT=3600000
DEFINES *= KEEP_ALIVE_TIME=0

CONFIG(debug, debug|release) {
#    DEFINES += NET_LIB_LOG
    DEFINES += AS_APP
    DEFINES -= AS_SERVICE
    message("--- debug ---")
}
else {
    DEFINES -= AS_APP
    DEFINES += AS_SERVICE
    message("--- release ---")
}

QMAKE_CXXFLAGS += -std=c++0x

INCLUDEPATH += ../../libcommon
INCLUDEPATH += ../../libcommon/system
INCLUDEPATH += ../../libcommon/log2
INCLUDEPATH += ../../libcommon/thread
INCLUDEPATH += ../../libcommon/net
INCLUDEPATH += ../../libcommon/otl4
INCLUDEPATH += ../../libcommon/obj_db5
INCLUDEPATH += ../../libcommon/xml
INCLUDEPATH += ../../libcommon/xml/lib
INCLUDEPATH += ../../libcommon/htmlreport
INCLUDEPATH += ../eq_common
INCLUDEPATH += /opt/ibm/db2/V9.7/include
INCLUDEPATH +=     ../../libcommon/sqlite3

SOURCES += \
    main.cpp \
    ../../libcommon/thread/clthread.cpp \
    ../../libcommon/log2/log.cpp \
    ../../libcommon/system/text_help.cpp \
    ../../libcommon/system/time.cpp \
    ../../libcommon/log2/log_file_writer.cpp \
    client_global.cpp \
    ../../libcommon/system/config_parser.cpp \
    ../eq_common/eq_net_client.cpp \
    ../../libcommon/xml/lib/ut_xml_par.cpp \
    ../../libcommon/xml/lib/ut_xml_tag.cpp \
    ../eq_common/eq_classes.cpp \
    ../../libcommon/net/net_cmd_client.cpp \
    ../eq_common/eq_net_auth.cpp \
    ../../libcommon/net/net_datagram_client.cpp \
    ../../libcommon/net/net_line_tcp.cpp \
    ../../libcommon/net/datagram.cpp \
    ../../libcommon/system/codepage.cpp \
    eq_client1.cpp

HEADERS += \
    ../../libcommon/thread/clthread.hpp \
    ../../libcommon/log2/log.hpp \
    ../../libcommon/log2/log_file_writer.hpp \
    ../../libcommon/system/error.hpp \
    ../../libcommon/system/udefs.hpp \
    ../../eq/eq_common/eq_common.hpp \
    ../../libcommon/system/time.hpp \
    client_global.hpp \
    ../../libcommon/system/config_parser.hpp \
    ../eq_common/eq_net_client.hpp \
    ../../libcommon/xml/ut_xml_par.hpp \
    ../../libcommon/xml/ut_xml_tag.hpp \
    ../eq_common/eq_classes.hpp \
    ../../libcommon/net/net_cmd_client.hpp \
    ../eq_common/eq_net_auth.hpp \
    ../../libcommon/net/net_datagram_client.hpp \
    ../../libcommon/net/net_line_tcp.h \
    ../../libcommon/net/net_line.h \
    ../../libcommon/net/datagram.hpp \
    ../../libcommon/system/codepage.hpp \
    ../../libcommon/system/text_help.hpp \
    eq_client1.hpp


#OTHER_FILES += \

#win32:LIBS += C:/Qt/Qt502/Tools/MinGW/i686-w64-mingw32/lib/libws2_32.a
unix:LIBS += /lib/librt.so.1
unix:LIBS += /lib/libpthread.so.0
unix:LIBS += /opt/ibm/db2/V9.7/lib32/libdb2.so
unix:LIBS += /lib/libdl.so.2

