#ifndef _CLIENT_GLOBAL_H_
#define _CLIENT_GLOBAL_H_

#include <ut_xml_par.hpp>
#include <string>

#include <eq_net_client.hpp>


// --------------------------------------------------
struct _view_xml
{
    ut_xml_par *parser;

    std::string root_path;

    _view_xml():parser(NULL),root_path("") {}
};

struct _run_config
{
    std::string run_path;
    std::string server_ip;
    std::string server_port;
    std::string uname;
    std::string upass;
    std::string log_levels;
    std::string log_file;
    std::string config_file_name;

};

// --------------------------------------------------
class _monitor_status
{
    private:

      //static _net_cmd_client *cm_mserver_client; //control_client *net_cc;
      static bool _need_exit;
      static _run_config cm_run_config;

      static _view_xml vxml;

      static _eq_net_client *cm_net_client;


    public:

      _eq_net_client *qserver_client() { return cm_net_client; }
      void qserver_client(_eq_net_client *s) { cm_net_client=s; }

      bool need_exit() const { return _need_exit; }
      void need_exit(bool s) { _need_exit = s; }

      _view_xml *view_xml() { return &vxml; }

      _run_config &run_config() { return cm_run_config; }
};


#endif // _CLIENT_GLOBAL_H_
