#ifndef EQ_CLIENT1_HPP
#define EQ_CLIENT1_HPP

#include <eq_net_client.hpp>

using namespace std;


class _client1:public _eq_net_client
{
private:
    string cm_user_name;
    string cm_user_pass;
protected:

    virtual void try_connect();
    virtual void try_login();
    virtual void get_jsondump();

    virtual int poll();


    virtual ~_client1();
public:
    _client1(const string &server,int port, const string &username, const string &password);

    virtual void release() { delete this; }

};



#endif // EQ_CLIENT1_HPP
